import { Address, Nat, string_to_mich } from '@completium/archetype-ts-types'
import { expect_to_fail, get_account, pack, set_mockup, set_mockup_now, set_quiet, get_big_map_value, set_endpoint } from '@completium/experiment-ts'

const assert = require('assert');

/* Contracts */

import { permits } from './binding/permits';
import { add_entrypoint_param, add_operator, exec_param, fa2, types__operator_param } from './binding/fa2';
import { transfer } from './binding/entrypoints/transfer';
import { update_operators } from './binding/entrypoints/update_operators';
import { balance_of } from './binding/entrypoints/balance_of';
import { mint, mint_param, mint_param_mich_type } from './binding/entrypoints/mint';
import { errors } from './binding/utils/errors';
import { transfer_destination, transfer_param } from './binding/types';

/* Accounts ----------------------------------------------------------------- */

const alice = get_account('alice');
const bob = get_account('bob');

/* Endpoint ---------------------------------------------------------------- */

set_mockup()
// set_endpoint("https://ghostnet.ecadinfra.com");

/* Verbose mode ------------------------------------------------------------ */

set_quiet(true);

/* Constants & Utils ------------------------------------------------------- */

const amount = new Nat(1000);
const token_id = new Nat(0);

/* Scenarios --------------------------------------------------------------- */

describe('[FA2] Contracts deployment', async () => {
  it('Permits contract deployment should succeed', async () => {
    await permits.deploy(alice.get_address(), { as: alice });
  });

  it('FA2 contract deployment should succeed', async () => {
    await fa2.deploy(alice.get_address(), permits.get_address(), { as: alice })
  });

  it('Transfer contract deployment should succeed', async () => {
    await transfer.deploy(fa2.get_address(), { as: alice })
  });

  it('Update Operators contract deployment should succeed', async () => {
    await update_operators.deploy(fa2.get_address(), { as: alice })
  });

  it('Balance Of contract deployment should succeed', async () => {
    await balance_of.deploy(fa2.get_address(), { as: alice })
  });

  it('Mint contract deployment should succeed', async () => {
    await mint.deploy(fa2.get_address(), { as: alice })
  });
});

describe('[FA2] Add entrypoints', async () => {
  it('Add Transfer contract should succeed', async () => {
    await fa2.add_entrypoint(
      new add_entrypoint_param('transfer', transfer.get_address()),
      { as: alice })
  });

  it('Add Update Operators contract should succeed', async () => {
    await fa2.add_entrypoint(
      new add_entrypoint_param('update_operators', update_operators.get_address()),
      { as: alice })
  });

  it('Add Balance Of contract should succeed', async () => {
    await fa2.add_entrypoint(
      new add_entrypoint_param('balance_of', balance_of.get_address()),
      { as: alice })
  });
  
  it('Add Mint contract should succeed', async () => {
    await fa2.add_entrypoint(
      new add_entrypoint_param('mint', mint.get_address()),
      { as: alice })
  });
});

describe('[FA2 multi-asset] Minting', async () => {
  it('Mint tokens as owner for ourself should succeed', async () => {
    const args = pack(
      new mint_param(
        alice.get_address(),      // owner
        token_id,                 // token id
        amount                    // amount
      ).to_mich(),
      mint_param_mich_type
    );

    await fa2.exec(new exec_param(
      'mint',
      args
    ), { as: alice });
  });

  it('Mint tokens as non owner for ourself should fail', async () => {
    const args = pack(
      new mint_param(
        alice.get_address(),      // owner
        token_id,                 // token id
        amount                    // amount
      ).to_mich(),
      mint_param_mich_type
    );

    await expect_to_fail(async () => {
      await fa2.exec(new exec_param(
        'mint',
        args
      ), { as: bob });
    }, string_to_mich("\"FA2_NOT_OWNER\""));
  });
});

describe('[FA2 multi-asset] Transfers', async () => {
  it('Transfer simple amount of token', async () => {
    const balance_before_user1 = await fa2.get_ledger_value([alice.get_address(), token_id]);
    assert(balance_before_user1?.equals(new Nat(1000)));
    const balance_before_user2 = await fa2.get_ledger_value([bob.get_address(), token_id]);
    assert(balance_before_user2 == undefined);

    const tps = [new transfer_param(alice.get_address(), [new transfer_destination(bob.get_address(), token_id, new Nat(1))])];

    await fa2.transfer(tps, { as: alice });

    const balance_after_user1 = await fa2.get_ledger_value([alice.get_address(), token_id]);
    assert(balance_after_user1?.equals(new Nat(999)));
    const balance_after_user2 = await fa2.get_ledger_value([bob.get_address(), token_id]);
    assert(balance_after_user2?.equals(new Nat(1)));
  });
});
