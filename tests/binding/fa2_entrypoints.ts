import * as ex from "@completium/experiment-ts";
import * as att from "@completium/archetype-ts-types";
export class fa2_storage implements att.ArchetypeType {
    constructor(public owner: att.Address, public permits: att.Address, public token_metadata: Array<[
        att.Nat,
        [
            att.Nat,
            Array<[
                att.Nat,
                att.Bytes
            ]>
        ]
    ]>, public ledger: Array<[
        [
            att.Address,
            att.Nat
        ],
        att.Nat
    ]>, public operator: Array<[
        [
            att.Address,
            att.Nat,
            att.Address
        ],
        att.Unit
    ]>, public operator_for_all: Array<[
        [
            att.Address,
            att.Address
        ],
        att.Unit
    ]>, public paused: boolean) { }
    toString(): string {
        return JSON.stringify(this, null, 2);
    }
    to_mich(): att.Micheline {
        return att.pair_to_mich([this.owner.to_mich(), this.permits.to_mich(), att.list_to_mich(this.token_metadata, x => {
                const x_key = x[0];
                const x_value = x[1];
                return att.elt_to_mich(x_key.to_mich(), att.pair_to_mich([x_value[0].to_mich(), att.list_to_mich(x_value[1], x => {
                        const x_key = x[0];
                        const x_value = x[1];
                        return att.elt_to_mich(x_key.to_mich(), x_value.to_mich());
                    })]));
            }), att.list_to_mich(this.ledger, x => {
                const x_key = x[0];
                const x_value = x[1];
                return att.elt_to_mich(att.pair_to_mich([x_key[0].to_mich(), x_key[1].to_mich()]), x_value.to_mich());
            }), att.list_to_mich(this.operator, x => {
                const x_key = x[0];
                const x_value = x[1];
                return att.elt_to_mich(att.pair_to_mich([x_key[0].to_mich(), x_key[1].to_mich(), x_key[2].to_mich()]), att.unit_to_mich());
            }), att.list_to_mich(this.operator_for_all, x => {
                const x_key = x[0];
                const x_value = x[1];
                return att.elt_to_mich(att.pair_to_mich([x_key[0].to_mich(), x_key[1].to_mich()]), att.unit_to_mich());
            }), att.bool_to_mich(this.paused)]);
    }
    equals(v: fa2_storage): boolean {
        return att.micheline_equals(this.to_mich(), v.to_mich());
    }
    static from_mich(input: att.Micheline): fa2_storage {
        return new fa2_storage(att.Address.from_mich((input as att.Mpair).args[0]), att.Address.from_mich((input as att.Mpair).args[1]), att.Int.from_mich((input as att.Mpair).args[2]), att.Int.from_mich((input as att.Mpair).args[3]), att.Int.from_mich((input as att.Mpair).args[4]), att.Int.from_mich((input as att.Mpair).args[5]), att.mich_to_bool((input as att.Mpair).args[6]));
    }
}
export class fa2_entrypoint_param implements att.ArchetypeType {
    constructor(public sender: att.Address, public params: att.Bytes, public storage: fa2_storage) { }
    toString(): string {
        return JSON.stringify(this, null, 2);
    }
    to_mich(): att.Micheline {
        return att.pair_to_mich([this.sender.to_mich(), this.params.to_mich(), this.storage.to_mich()]);
    }
    equals(v: fa2_entrypoint_param): boolean {
        return att.micheline_equals(this.to_mich(), v.to_mich());
    }
    static from_mich(input: att.Micheline): fa2_entrypoint_param {
        return new fa2_entrypoint_param(att.Address.from_mich((input as att.Mpair).args[0]), att.Bytes.from_mich((input as att.Mpair).args[1]), fa2_storage.from_mich(att.pair_to_mich((input as att.Mpair as att.Mpair).args.slice(2, 9))));
    }
}
export const fa2_storage_mich_type: att.MichelineType = att.pair_array_to_mich_type([
    att.prim_annot_to_mich_type("address", ["%owner"]),
    att.prim_annot_to_mich_type("address", ["%permits"]),
    att.pair_annot_to_mich_type("big_map", att.prim_annot_to_mich_type("nat", []), att.pair_array_to_mich_type([
        att.prim_annot_to_mich_type("nat", []),
        att.pair_annot_to_mich_type("map", att.prim_annot_to_mich_type("nat", []), att.prim_annot_to_mich_type("bytes", []), [])
    ], []), ["%token_metadata"]),
    att.pair_annot_to_mich_type("big_map", att.pair_array_to_mich_type([
        att.prim_annot_to_mich_type("address", []),
        att.prim_annot_to_mich_type("nat", [])
    ], []), att.prim_annot_to_mich_type("nat", []), ["%ledger"]),
    att.pair_annot_to_mich_type("big_map", att.pair_array_to_mich_type([
        att.prim_annot_to_mich_type("address", []),
        att.prim_annot_to_mich_type("nat", []),
        att.prim_annot_to_mich_type("address", [])
    ], []), att.prim_annot_to_mich_type("unit", []), ["%operator"]),
    att.pair_annot_to_mich_type("big_map", att.pair_array_to_mich_type([
        att.prim_annot_to_mich_type("address", []),
        att.prim_annot_to_mich_type("address", [])
    ], []), att.prim_annot_to_mich_type("unit", []), ["%operator_for_all"]),
    att.prim_annot_to_mich_type("bool", ["%paused"])
], []);
export const fa2_entrypoint_param_mich_type: att.MichelineType = att.pair_array_to_mich_type([
    att.prim_annot_to_mich_type("address", ["%sender"]),
    att.prim_annot_to_mich_type("bytes", ["%params"]),
    att.pair_array_to_mich_type([
        att.prim_annot_to_mich_type("address", ["%owner"]),
        att.prim_annot_to_mich_type("address", ["%permits"]),
        att.pair_annot_to_mich_type("big_map", att.prim_annot_to_mich_type("nat", []), att.pair_array_to_mich_type([
            att.prim_annot_to_mich_type("nat", []),
            att.pair_annot_to_mich_type("map", att.prim_annot_to_mich_type("nat", []), att.prim_annot_to_mich_type("bytes", []), [])
        ], []), ["%token_metadata"]),
        att.pair_annot_to_mich_type("big_map", att.pair_array_to_mich_type([
            att.prim_annot_to_mich_type("address", []),
            att.prim_annot_to_mich_type("nat", [])
        ], []), att.prim_annot_to_mich_type("nat", []), ["%ledger"]),
        att.pair_annot_to_mich_type("big_map", att.pair_array_to_mich_type([
            att.prim_annot_to_mich_type("address", []),
            att.prim_annot_to_mich_type("nat", []),
            att.prim_annot_to_mich_type("address", [])
        ], []), att.prim_annot_to_mich_type("unit", []), ["%operator"]),
        att.pair_annot_to_mich_type("big_map", att.pair_array_to_mich_type([
            att.prim_annot_to_mich_type("address", []),
            att.prim_annot_to_mich_type("address", [])
        ], []), att.prim_annot_to_mich_type("unit", []), ["%operator_for_all"]),
        att.prim_annot_to_mich_type("bool", ["%paused"])
    ], ["%storage"])
], []);
export class Fa2_entrypoints {
    address: string | undefined;
    constructor(address: string | undefined = undefined) {
        this.address = address;
    }
    get_address(): att.Address {
        if (undefined != this.address) {
            return new att.Address(this.address);
        }
        throw new Error("Contract not initialised");
    }
    async get_balance(): Promise<att.Tez> {
        if (null != this.address) {
            return await ex.get_balance(new att.Address(this.address));
        }
        throw new Error("Contract not initialised");
    }
    async deploy(params: Partial<ex.Parameters>) {
        const address = (await ex.deploy("./contracts/fa2_entrypoints.arl", {}, params)).address;
        this.address = address;
    }
    errors = {};
}
export const fa2_entrypoints = new Fa2_entrypoints();
