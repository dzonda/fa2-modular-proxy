archetype balance_of(fa2 : address)

import "../types.arl"
import "../utils/errors.arl"

/* FUNCTIONS --------------------------------------------------------------- */

function get_balance(br : types::balance_of_request, s : types::Storage) : nat {
  do_require(contains(s.token_metadata, br.btoken_id), errors::FA2_TOKEN_UNDEFINED);
  return (s.ledger[(br.bo_owner, br.btoken_id)] ? the : 0)
}

entry balance_of (p : types::balance_of_param, s : types::Storage) {
  called by self_address

  effect {
    const balances = map(p.requests, br ->
      types::{
        request = br;
        balance_ = get_balance(br, s)
      });

    const callback ?= address_to_contract<list<types::balance_of_response>>(p.callback) : "Invalid callback contract";
    transfer transferred to entry callback(balances)
  }
}

entry default (p : types::proxy_execute_param) {
  no transfer
  called by fa2

  effect {
    const balance_of_param ?= unpack<types::balance_of_param>(p.pe_params) : "UNPACK_FAILED: Invalid balance_of parameters";

    transfer transferred to entry self.balance_of(balance_of_param, p.pe_storage)
  }
}
