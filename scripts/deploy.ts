import { get_account, set_quiet, set_endpoint } from '@completium/experiment-ts'

/* Contracts */

import { permits } from '../tests/binding/permits';
import { add_entrypoint_param, fa2 } from '../tests/binding/fa2';
import { transfer } from '../tests/binding/entrypoints/transfer';
import { update_operators } from '../tests/binding/entrypoints/update_operators';
import { balance_of } from '../tests/binding/entrypoints/balance_of';
import { mint } from '../tests/binding/entrypoints/mint';

/* Accounts ---------------------------------------------------------------- */

const alice = get_account('alice');

/* Endpoint ---------------------------------------------------------------- */

set_endpoint("https://ghostnet.ecadinfra.com");

/* Verbose mode ------------------------------------------------------------ */

set_quiet(false);

/* Scenarios --------------------------------------------------------------- */

describe('Contracts deployment', async () => {
  it('Permits contract deployment should succeed', async () => {
    await permits.deploy(alice.get_address(), { as: alice });

    console.log(`Permits address: ${permits.get_address()}`);
  });

  it('FA2 contract deployment should succeed', async () => {
    await fa2.deploy(alice.get_address(), permits.get_address(), { as: alice });

    console.log(`FA2 address: ${fa2.get_address()}`);
  });

  it('Transfer contract deployment should succeed', async () => {
    await transfer.deploy(fa2.get_address(), { as: alice });

    console.log(`Transfer address: ${transfer.get_address()}`);
  });

  it('Update Operators contract deployment should succeed', async () => {
    await update_operators.deploy(fa2.get_address(), { as: alice })

    console.log(`Update Operators address: ${update_operators.get_address()}`);
  });

  it('Balance Of contract deployment should succeed', async () => {
    await balance_of.deploy(fa2.get_address(), { as: alice })

    console.log(`Balance Of address: ${balance_of.get_address()}`);
  });

  it('Mint contract deployment should succeed', async () => {
    await mint.deploy(fa2.get_address(), { as: alice });

    console.log(`Mint address: ${mint.get_address()}`);
  });
});

describe('Add entrypoints', async () => {
  it('Add Transfer contract should succeed', async () => {
    await fa2.add_entrypoint(
      new add_entrypoint_param('transfer', transfer.get_address()),
      { as: alice })
  });

  it('Add Update Operators contract should succeed', async () => {
    await fa2.add_entrypoint(
      new add_entrypoint_param('update_operators', update_operators.get_address()),
      { as: alice })
  });

  it('Add Balance Of contract should succeed', async () => {
    await fa2.add_entrypoint(
      new add_entrypoint_param('balance_of', balance_of.get_address()),
      { as: alice })
  });
  
  it('Add Mint contract should succeed', async () => {
    await fa2.add_entrypoint(
      new add_entrypoint_param('mint', mint.get_address()),
      { as: alice })
  });
});
