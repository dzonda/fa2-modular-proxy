import { get_account, set_quiet, set_endpoint } from '@completium/experiment-ts'

/* Contracts */

import { Fa2, add_entrypoint_param } from '../tests/binding/fa2';
import { transfer } from '../tests/binding/entrypoints/transfer';
import { update_operators } from '../tests/binding/entrypoints/update_operators';
import { balance_of } from '../tests/binding/entrypoints/balance_of';
import { mint } from '../tests/binding/entrypoints/mint';

/* Accounts ---------------------------------------------------------------- */

const alice = get_account('alice');

/* Endpoint ---------------------------------------------------------------- */

set_endpoint("https://ghostnet.ecadinfra.com");

/* Verbose mode ------------------------------------------------------------ */

set_quiet(true);

/* Entrypoints ------------------------------------------------------------- */

const entrypoints: Record<string, any> = {
  'transfer': transfer,
  'update_operators': update_operators,
  'balance_of': balance_of,
  'mint': mint
};

/* Scenarios --------------------------------------------------------------- */

const fa2_address = process.env.FA2_ADDRESS;
const argument = process.env.ENTRYPOINT;

if (fa2_address === undefined) {
  console.log('Please provide a FA2 address');
  process.exit(1);
}

if (argument === undefined) {
  console.log('Please provide an entrypoint to update');
  process.exit(1);
}

const entrypoint = entrypoints[argument];
if (entrypoint === undefined) {
  console.log('Entrypoint not found');
  process.exit(1);
}

describe(`Update entrypoint: ${argument}`, async () => {
  it(`Contract deployment should succeed`, async () => {
    await entrypoint.deploy(alice.get_address(), { as: alice });
    console.log(`New entrypoint address for ${argument}: ${entrypoint.get_address()}`);
  });

  it(`Add entrypoint ${argument} should succeed`, async () => {
    const fa2 = new Fa2(fa2_address);

    await fa2.add_entrypoint(
      new add_entrypoint_param(argument, entrypoint.get_address()),
      { as: alice }
    );
    console.log(`Entrypoint ${argument} updated`);
  });
});
